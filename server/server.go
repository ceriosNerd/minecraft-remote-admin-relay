package server

import (
	"log"
	"net"
	"crypto/tls"
	"bufio"
	"strings"
	"crypto/x509"
	"io/ioutil"
	"crypto/rand"
	"crypto/rsa"
	"time"
	"crypto/x509/pkix"
	"math/big"
	"os"
	"fmt"
)

var pem string
var key string

func genCert() {
	ca := &x509.Certificate{
		SerialNumber: big.NewInt(1337),
		Subject: pkix.Name{
			Country:            []string{"Neuland"},
			Organization:       []string{"qwertz"},
			OrganizationalUnit: []string{"qwertz"},
		},
		Issuer: pkix.Name{
			Country:            []string{"Neuland"},
			Organization:       []string{"Skynet"},
			OrganizationalUnit: []string{"Computer Emergency Response Team"},
			Locality:           []string{"Neuland"},
			Province:           []string{"Neuland"},
			StreetAddress:      []string{"Mainstreet 23"},
			PostalCode:         []string{"12345"},
			SerialNumber:       "23",
			CommonName:         "23",
		},
		SignatureAlgorithm:    x509.SHA512WithRSA,
		PublicKeyAlgorithm:    x509.ECDSA,
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(0, 0, 10),
		SubjectKeyId:          []byte{1, 2, 3, 4, 5},
		BasicConstraintsValid: true,
		IsCA:        true,
		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:    x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
	}

	priv, _ := rsa.GenerateKey(rand.Reader, 4096)
	pub := &priv.PublicKey
	ca_b, err := x509.CreateCertificate(rand.Reader, ca, ca, pub, priv)
	if err != nil {
		log.Fatalf("create cert failed %#v", err)
		return
	}
	log.Println("save", pem)
	ioutil.WriteFile(pem, ca_b, 0644)
	log.Println("save", key)
	ioutil.WriteFile(key, x509.MarshalPKCS1PrivateKey(priv), 0644)
}

func StartServer() {
	pem = "cert.pem"
	key = "cert.key"
	if _, err := os.Stat(pem); os.IsNotExist(err) {
		if _, err := os.Stat(key); os.IsNotExist(err) {
			fmt.Println("no certs found, generating new self signed certs.")
			genCert()
		}
	}
	ca_b, _ := ioutil.ReadFile("cert.pem")
	ca, _ := x509.ParseCertificate(ca_b)
	priv_b, _ := ioutil.ReadFile("cert.key")
	priv, _ := x509.ParsePKCS1PrivateKey(priv_b)
	pool := x509.NewCertPool()
	pool.AddCert(ca)

	cert := tls.Certificate{
		Certificate: [][]byte{ca_b},
		PrivateKey:  priv,
	}
	config := tls.Config{
		Certificates: []tls.Certificate{cert},
		//MinVersion:   tls.VersionSSL30, //don't use SSLv3, https://www.openssl.org/~bodo/ssl-poodle.pdf
		MinVersion: tls.VersionTLS10,
		//MinVersion:   tls.VersionTLS11,
		//MinVersion:   tls.VersionTLS12,
	}
	config.Rand = rand.Reader

	// TODO move port to config
	p := ":1337"
	tcpAddr, resolveTcpAddrErr := net.ResolveTCPAddr("tcp", p)

	if resolveTcpAddrErr != nil {
		log.Fatal("Error: Could not resolve address", p)
		return
	}
	// start to listen connections
	listener, listenerError := tls.Listen(tcpAddr.Network(), tcpAddr.String(), &config)

	if listenerError != nil {
		log.Fatal("Error: TCP listener couldn't start", listenerError)
		return
	}

	defer listener.Close()

	// start to accept incoming connections
	log.Println("Listening for connections")
	for {
		conn, acceptErr := listener.Accept()

		if acceptErr != nil {
			log.Println("Error: can't accept new connection")
		} else {
			go handleHandshake(conn)
		}
	}
}

func sendMessage(writer *bufio.Writer, message string) {
	writer.WriteString(message + "\n")
	writer.Flush()
}

func handleHandshake(conn net.Conn) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	line, err := reader.ReadBytes('\n')
	if (err != nil) {
		return
	}

	handshakeMessage := strings.TrimSuffix(string(line), "\n")
	split := strings.Split(handshakeMessage, ":")
	if (len(split) < 2) {
		sendMessage(writer, "ERR: Goodbye!")
		conn.Close()
	}
	switch split[0] {
	case "HELO":
		if (split[1] != strings.Split(conn.RemoteAddr().String(), ":")[0]) {
			log.Println("Host mismatch! " + split[1] + " =/= " + strings.Split(conn.RemoteAddr().String(), ":")[0])
			sendMessage(writer, "ERR: Host mismatch! Goodbye!")
			conn.Close()
			return
		} else {
			sendMessage(writer, "ADMINKEY?")
			adminkey, err := reader.ReadBytes('\n')
			if (err != nil) {
				return
			}
			log.Println("Admin Server connected: " + conn.RemoteAddr().String())
			RegisterServer(strings.TrimSuffix(string(adminkey), "\n"), &conn)
		}
	case "CLIENT":
		log.Println("Remote Admin client connected with device ID: " + split[1])
		sendMessage(writer, "ADMINKEY?")
		message, err := reader.ReadBytes('\n')
		if (err != nil) {
			log.Println(err.Error())
			return
		}

		keyToTest := strings.TrimSuffix(string(message), "\n")
		if _, ok := servers[keyToTest]; !ok {
			sendMessage(writer, "ERR: Goodbye!")
			conn.Close()
			return
		}
		servers[keyToTest].RegisterClient(&conn, split[1])
		break
	}
}