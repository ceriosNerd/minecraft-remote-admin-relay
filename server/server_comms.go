package server
import (
	"net"
	"bufio"
	"strings"
	"time"
	"log"
	"sync"
)

type ConnectionStatus int

const (
	ALIVE ConnectionStatus = iota
	UNKNOWN
	DEAD
)

type Connection interface  {
	getConn() *net.Conn
	getReader() chan string
	getWriter() chan string
	getStatus() ConnectionStatus
	setStatus(ConnectionStatus)
	getLastSeen() time.Time
	seen()
}

type AdminServer struct {
	adminKey string
	server *net.Conn
	reader chan string
	writer chan string
	addClient chan ClientConnection
	clients map[string]*ClientConnection
	lastSeen time.Time
	status ConnectionStatus
}

func (server *AdminServer) getReader() chan string {
	return server.reader
}
func (server *AdminServer) getWriter() chan string {
	return server.writer
}
func (server *AdminServer) getConn() *net.Conn {
	return server.server
}
func (server *AdminServer) getStatus() ConnectionStatus {
	return server.status
}
func (server *AdminServer) setStatus(state ConnectionStatus) {
	server.status = state;
}
func (server *AdminServer) getLastSeen() time.Time {
	return server.lastSeen
}
func (server *AdminServer) seen() {
	if (server.status == UNKNOWN) {
		server.status = ALIVE
	}
	server.lastSeen = time.Now()
}

type ClientConnection struct {
	client *net.Conn
	clientId string
	conenctedParent *AdminServer
	reader chan string
	writer chan string
	parentWriter chan string
	lastSeen time.Time
	authed sync.WaitGroup
	status ConnectionStatus
}

func (client *ClientConnection) getReader() chan string {
	return client.reader
}
func (client *ClientConnection) getWriter() chan string {
	return client.writer
}
func (client *ClientConnection) getConn() *net.Conn {
	return client.client
}
func (client *ClientConnection) getStatus() ConnectionStatus {
	return client.status
}
func (client *ClientConnection) setStatus(state ConnectionStatus) {
	client.status = state;
}
func (client *ClientConnection) getLastSeen() time.Time {
	return client.lastSeen
}
func (client *ClientConnection) seen() {
	if (client.status == UNKNOWN) {
		client.status = ALIVE
	}
	client.lastSeen = time.Now()
}

var servers map[string]*AdminServer = make(map[string]*AdminServer)

func RegisterServer(key string, host *net.Conn) {
	servers[key] = &AdminServer{key, host, make(chan string), make(chan string), make(chan ClientConnection), make(map[string]*ClientConnection), time.Now(), ALIVE}
	go messageReceiver(servers[key])
	go messageSender(servers[key])
	go serverHandler(servers[key])
	go servers[key].clientAdditionHandler()
}

func closeConnection(connection Connection) {
	log.Println("[" + (*connection.getConn()).RemoteAddr().String() + "] Disconnected!")
	connection.setStatus(DEAD)
}

func (parentServer *AdminServer) RegisterClient(clientConnection *net.Conn, clientId string) {
	parentServer.addClient <- ClientConnection{clientConnection, clientId, parentServer, make(chan string), make(chan string), make(chan string), time.Now(), *new(sync.WaitGroup), ALIVE}
}

func messageSender(connection Connection) {
	writer := bufio.NewWriter(*connection.getConn())
	for message := range connection.getWriter() {
		if (connection.getStatus() == DEAD) {
			return
		}
		sendMessage(writer, message)
	}
}

func messageReceiver(connection Connection) {
	reader := bufio.NewReader(*connection.getConn())
	for connection.getStatus() != DEAD {
		messageBytes, err := reader.ReadBytes('\n')
		if (err != nil) {
			closeConnection(connection)
			return
		}

		// We know that the other end is still live by receiving this, update last seen
		connection.seen()

		// Tell channel listeners that there is a new message
		connection.getReader() <- strings.TrimSuffix(string(messageBytes), "\n")
	}
}

func (client *ClientConnection) parentMessagePoll() {
	for message := range client.parentWriter {
		if (client.status == DEAD) {
			return
		}
		// Client cannot send anything till it has been authed
		client.authed.Wait()
		client.conenctedParent.writer <- message
	}
}

func (server *AdminServer) clientAdditionHandler() {
	for client := range server.addClient {
		server.clients[client.clientId] = &client
		client.authed.Add(1)
		go messageSender(&client)
		go messageReceiver(&client)
		go remoteClientHandler(&client)
		go client.parentMessagePoll()
		// Lets check if they are allowed to connect
		server.writer <- "ALLOW:" + client.clientId
	}
}

func keepAliveHandler(connection Connection) *time.Ticker {
	keepAlive := time.NewTicker(time.Minute * 15)
	go func() {
		for range keepAlive.C {
			if connection.getStatus() == DEAD {
				return
			}
			if time.Since(connection.getLastSeen()) > (time.Minute * 15) {
				connection.getWriter() <- "PING"
				connection.setStatus(UNKNOWN)
			}
		}
	}()
	return keepAlive
}

func serverHandler(server *AdminServer) {
	for message := range server.reader {
		split := strings.Split(message, ":")
		var command string
		if len(split) <= 1 {
			command = message
		} else {
			command = split[0]
		}

		switch command {
		case "PING": // This is our keep alive system
			server.writer <- "ACK"
		case "BYE":
			closeConnection(server)
		case "DENY":
			if len(split) < 2 {
				break
			}

			if _, ok := server.clients[split[1]]; ok {
				server.writer <- "ERR: Denied Access!"
				closeConnection(server.clients[split[1]])
				delete(server.clients, split[1])
				log.Println("Client: " + split[1] + " was denied access to a server!")
			}
		case "ALLOW":
			if len(split) < 2 {
				break
			}
			if _, ok := server.clients[split[1]]; ok {
				client := server.clients[split[1]]
				client.writer <- "AUTH: OK"
				client.authed.Done()
				log.Println("Client: " + split[1] + " was allowed access to a server!")
			}
		case "REAUTH":
			if len(split) < 2 {
				break
			}
			deniedCient := strings.Split(split[1], "\r\n")
			if len(deniedCient) < 2 {
				break
			}

			if client, ok := server.clients[deniedCient[0]]; ok {
				client.authed.Add(1) // Lock the client from communicating to the server
				client.writer <- "REAUTH" // Ask the client for it's Id and admin key again
				client.parentWriter <- deniedCient[1] // Queue the rejected command to resend first thing after auth
			}
		case "CONSOLE":
			// Relay the message
			go func() {
				for _, client := range server.clients {
					client.writer <- message
				}
			}()
		}
	}
}

func remoteClientHandler(client *ClientConnection) {
	defer keepAliveHandler(client).Stop()

	for message := range client.reader {
		split := strings.Split(message, ":")
		var command string
		if len(split) <= 1 {
			command = message
		} else {
			command = split[0]
		}

		switch command {
		case "PING": // This is our keep alive system
			client.writer <- "ACK"
		case "BYE":
			closeConnection(client)
		case "REAUTH":
			if len(split) < 2 {
				break
			}
			authParams := strings.Split(split[1], "\r")
			if len(authParams) < 2 {
				break
			}
			if client.clientId != authParams[0] {
				closeConnection(client)
				return
			}
			if client.conenctedParent.adminKey != authParams[1] {
				closeConnection(client)
				return
			}
			// This is the only client method that always will be able to communicate with the server with the condition of matching admin keys
			client.conenctedParent.writer <- "ALLOW:" + client.clientId
		case "CMD":
			if len(split) < 2 {
				break
			}
			// Use the carriage return character to split the client id and the command to be relayed
			client.parentWriter <- "CMD:" + client.clientId + "\r" + split[1]
		case "STATUS":
			client.parentWriter <- "STATUS:" + client.clientId
		}
	}
}